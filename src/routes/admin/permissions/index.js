import React from 'react';
import AdminLayout from '../../../components/Layout/AdminLayout';

export default {
  path: '/permissions',
  children: [
    {
      path: '/roles',
      children: [
        {
          path: '',
          async action() {
            const {
              default: RolePage,
            } = await import(/* webpackChunkName: 'RolePage' */ './components/RolePage');
            return {
              chunk: ['RolePage'],
              title: 'Roles',
              component: (
                <AdminLayout>
                  <RolePage />
                </AdminLayout>
              ),
            };
          },
        },
        {
          path: '/new',
          async action() {
            const {
              default: RoleForm,
            } = await import(/* webpackChunkName: 'RoleForm' */ './components/RoleForm');
            return {
              chunk: ['RoleForm'],
              title: 'New Role',
              component: (
                <AdminLayout>
                  <RoleForm />
                </AdminLayout>
              ),
            };
          },
        },
        {
          path: '/edit/:id',
          async action(context, params) {
            const {
              default: RoleForm,
            } = await import(/* webpackChunkName: 'RoleForm' */ './components/RoleForm');
            return {
              chunk: ['RoleForm'],
              title: 'Edit Role',
              component: (
                <AdminLayout>
                  <RoleForm params={params}/>
                </AdminLayout>
              ),
            };
          },
        },
      ],
    },
    {
      path: '',
      async action() {
        const {
          default: PermissionPage,
        } = await import(/* webpackChunkName: 'PermissionPage' */ './components/PermissionPage');
        return {
          chunk: ['PermissionPage'],
          title: 'Permissions',
          component: (
            <AdminLayout>
              <PermissionPage />
            </AdminLayout>
          ),
        };
      },
    },
    {
      path: '/new',
      async action() {
        const {
          default: PermissionForm,
        } = await import(/* webpackChunkName: 'PermissionForm' */ './components/PermissionForm');
        return {
          chunks: ['PermissionForm'],
          title: 'New Permission',
          component: (
            <AdminLayout>
              <PermissionForm />
            </AdminLayout>
          ),
        };
      },
    },
    {
      path: '/edit/:id',
      async action(context, params) {
        const {
          default: PermissionForm,
        } = await import(/* webpackChunkName: 'PermissionForm' */ './components/PermissionForm');
        return {
          chunks: ['PermissionForm'],
          title: 'Edit Permission',
          component: (
            <AdminLayout>
              <PermissionForm params={params} />
            </AdminLayout>
          ),
        };
      },
    },
  ],
};
