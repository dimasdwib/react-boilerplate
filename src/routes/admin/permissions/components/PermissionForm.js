import React from 'react';
import axios from 'axios';
import history from '../../../../history';
import TextField from '../../../../components/Form/TextField';
import LoadingButton from '../../../../components/Button/LoadingButton';
import Link from '../../../../components/Link';
import ReactSelect from '../../../../components/Form/ReactSelect';

class PermissionForm extends React.Component {
  static defaultProps = {
    params: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      group: '',
      role: [],
      user: [],
      isLoadingSave: '',
      roleData: [],
      userData: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.fetchPermission = this.fetchPermission.bind(this);
    this.fetchRole = this.fetchRole.bind(this);
    this.handleRole = this.handleRole.bind(this);
    this.handleUser = this.handleUser.bind(this);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.fetchPermission(this.props.params.id);
    }
    this.fetchRole();
    this.fetchUser();
  }

  /**
   * Fetch single permission
   */
  fetchPermission(id) {
    axios.get(`/permission/get/${id}`)
    .then((res) => {
      this.setState({
        ...res.data.data,
      });
    })
    .catch((err) => {
      this.props.notify('error', err.response.data.message);
    });
  }

  fetchRole() {
    axios.get('permission/role/all')
    .then(res => {
      this.setState({
        roleData: res.data,
      });
    })
    .catch(err => {

    });
  }

  fetchUser() {
    axios.get('user/all')
    .then(res => {
      this.setState({
        userData: res.data,
      });
    })
    .catch(err => {

    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleRole(e) {
    this.setState({
      role: e,
    });
  }

  handleUser(e) {
    this.setState({
      user: e,
    });
  }

  handleSubmit(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    const { name, group, role, user } = this.state;
    const roleArr = [];
    const userArr = [];
    role.forEach(r => {
      roleArr.push(r.label);
    });
    user.forEach(u => {
      userArr.push(u.value);
    });
    const data = {
      name,
      group,
      role: roleArr.join(','),
      user: userArr.join(','),
    };

    // console.log(data);
    //
    // return;

    let url = '/permission/store';
    if (this.props.params.id) {
      data.id = this.props.params.id;
      url = `/permission/update/${this.props.params.id}`;
    }

    this.setState({ isLoadingSave: true });
    axios.post(url, data)
    .then(res => {
      this.props.notify('success', res.data.message);
      this.setState({ isLoadingSave: false });
      history.push({
        pathname: `/admin/permissions/edit/${res.data.id}`,
      });
    })
    .catch(err => {
      this.props.notify('error', err.response.data.message);
      this.setState({ isLoadingSave: false });
    });
  }

  render() {
    const { group, name, roleData, userData, user, role } = this.state;
    let title = 'Create Permission';
    let buttonLabel = 'Save';
    if (this.props.params.id) {
      title = 'Edit Permission';
      buttonLabel = 'Update';
    }

    const roleOpt = [];
    const userOpt = [];
    roleData.forEach(r => {
      roleOpt.push({
        value: r.id,
        label: r.name,
      });
    });
    userData.forEach(u => {
      userOpt.push({
        value: u.id,
        label: u.name,
      });
    });

    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-12">
                    <h4 className="card-title mb-0">{ title }</h4>
                    <br />
                  </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <TextField
                        labelText="Name"
                        name="name"
                        onChange={this.handleChange}
                        value={name}
                        placeholder="Name"
                      />
                    </div>
                    {/*
                    <div className="form-group">
                      <TextField
                        labelText="Group"
                        name="group"
                        onChange={this.handleChange}
                        value={group}
                        placeholder="Permission Group"
                      />
                    </div>
                    */}
                    <div>
                      <LoadingButton
                        className="btn btn-primary"
                        onClick={this.handleSubmit}
                        isLoading={this.state.isLoadingSave}
                      >
                        {buttonLabel}
                      </LoadingButton>
                      &nbsp;
                      &nbsp;
                      <Link to="/admin/permissions">
                        <button disabled={this.state.isLoadingSave} className="btn btn-default">
                          Back
                        </button>
                      </Link>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <ReactSelect
                        labelText="Roles"
                        isMulti
                        value={role}
                        options={roleOpt}
                        onChange={this.handleRole}
                      />
                    </div>
                    <div className="form-group">
                      <ReactSelect
                        labelText="Users"
                        isMulti
                        options={userOpt}
                        value={user}
                        onChange={this.handleUser}
                      />
                    </div>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PermissionForm;
