import React from 'react';
import axios from 'axios';
import { SyncLoader } from 'react-spinners';
import Link from '../../../../components/Link';
import TabMenu from './TabMenu';

class PermissionPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchPermission: 'DONE',
      permissionData: {},
    };
    this.fetchPermission = this.fetchPermission.bind(this);
    this.deletePermission = this.deletePermission.bind(this);
  }

  componentDidMount() {
    this.fetchPermission();
  }

  deletePermission(permission) {
    this.props.confirm(`Are you sure want to delete ${permission.name} ?`)
    .then(() => {
      axios.post(`/permission/delete/${permission.name}`)
      .then(res => {
        this.fetchPermission();
        this.props.notify('success', res.data.message);
      })
      .catch(err => {
        this.props.notify('error', err.response.data.message);
      });
    });
  }

  fetchPermission() {
    this.setState({fetchPermission: 'LOADING'});
    axios.get('/permission')
    .then(res => {
      this.setState({
        permissionData: res.data,
        fetchPermission: 'DONE',
      });
    })
    .catch(err => {
      this.setState({fetchPermission: 'ERROR'});
    });
  }

  render() {
    const { permissionData, fetchPermission } = this.state;
    const rowData = [];
    if (fetchPermission === 'LOADING') {
      rowData.push(
        <tr key="loading">
          <td className="text-center" colSpan="6">
            <SyncLoader size={10} color="#d0d0d0" />
          </td>
        </tr>
      );
    }
    if (permissionData.data) {
      permissionData.data.forEach((permission, i) => {
        rowData.push(
          <tr key={i}>
            <td> {i + 1} </td>
            <td> {permission.name} </td>
            <td> - </td>
            <td>
              <Link to={`/admin/permissions/edit/${permission.id}`}><button className="btn btn-primary btn-sm"> <i className="fa fa-edit fa-fw" /> </button></Link>
              &nbsp;
              <button onClick={() => this.deletePermission(permission)} className="btn btn-danger btn-sm"> <i className="fa fa-trash fa-fw" /> </button>
            </td>
          </tr>
        );
      });
    }
    return (
      <div className="animated fadeIn">
        <TabMenu active="permissions" />
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="form-group">
                  <Link to="/admin/permissions/new">
                    <button className="btn btn-primary"> Create New </button>
                  </Link>
                </div>
                <div className="table-responsive">
                  <table className="table table-compact">
                    <thead>
                      <tr>
                        <th> No </th>
                        <th> Name </th>
                        <th> Group </th>
                        <th> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      {rowData}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PermissionPage;
