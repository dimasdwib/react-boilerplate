import React from 'react';
import Link from '../../../../components/Link';

class TabMenu extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <div className="form-group">
            <Link to="/admin/permissions"><button className={this.props.active === 'permissions' ? 'btn btn-primary' : 'btn'}> Permissions </button></Link>
            &nbsp;
            <Link to="/admin/permissions/roles"><button className={this.props.active === 'roles' ? 'btn btn-primary' : 'btn'}> Roles </button></Link>
          </div>
        </div>
      </div>
    );
  }
}

export default TabMenu;
