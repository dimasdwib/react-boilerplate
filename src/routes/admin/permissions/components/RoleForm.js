import React from 'react';
import axios from 'axios';
import history from '../../../../history';
import TextField from '../../../../components/Form/TextField';
import LoadingButton from '../../../../components/Button/LoadingButton';
import Link from '../../../../components/Link';
import ReactSelect from '../../../../components/Form/ReactSelect';

class RoleForm extends React.Component {
  static defaultProps = {
    params: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      isLoadingSave: '',
      permission: [],
      user: [],
      permissionData: [],
      userData: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.fetchRole = this.fetchRole.bind(this);
    this.fetchUser = this.fetchUser.bind(this);
    this.fetchPermission = this.fetchPermission.bind(this);
    this.handlePermission = this.handlePermission.bind(this);
    this.handleUser = this.handleUser.bind(this);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.fetchRole(this.props.params.id);
    }
    this.fetchUser();
    this.fetchPermission();
  }

  /**
   * Fetch single permission
   */
  fetchRole(id) {
    axios.get(`/permission/role/get/${id}`)
    .then((res) => {
      this.setState({
        ...res.data.data,
      });
    })
    .catch((err) => {
      this.props.notify('error', err.response.data.message);
    });
  }

  fetchPermission() {
    axios.get(`/permission/all`)
    .then(res => {
      this.setState({
        permissionData: res.data,
      });
    })
    .catch(err => {
      this.props.notify('error', err.response.data.message);
    });
  }

  fetchUser() {
    axios.get(`/user/all`)
    .then(res => {
      this.setState({
        userData: res.data,
      });
    })
    .catch(err => {
      this.props.notify('error', err.response.data.message);
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleUser(e) {
    this.setState({
      user: e,
    });
  }

  handlePermission(e) {
    this.setState({
      permission: e,
    });
  }

  handleSubmit(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    const { name, user, permission } = this.state;
    const u = [];
    const p = [];
    permission.forEach(d => {
      p.push(d.label);
    });
    user.forEach(d => {
      u.push(d.value);
    });
    const data = {
      name,
      user: u.join(','),
      permission: p.join(','),
    };

    // console.log(data);
    // return;

    let url = '/permission/role/store';
    if (this.props.params.id) {
      data.id = this.props.params.id;
      url = `/permission/role/update/${this.props.params.id}`;
    }

    this.setState({ isLoadingSave: true });
    axios.post(url, data)
    .then(res => {
      this.props.notify('success', res.data.message);
      this.setState({ isLoadingSave: false });
      history.push({
        pathname: `/admin/permissions/roles/edit/${res.data.id}`,
      });
    })
    .catch(err => {
      this.props.notify('success', err.response.data.message);
      this.setState({ isLoadingSave: false });
    });
  }

  render() {
    const { name, permissionData, userData, permission, user } = this.state;
    let title = 'Create Role';
    let buttonLabel = 'Save';
    if (this.props.params.id) {
      title = 'Edit Role';
      buttonLabel = 'Update';
    }

    const permissionOpt = [];
    const userOpt = [];
    permissionData.forEach(p => {
      permissionOpt.push({
        label: p.name,
        value: p.id,
      });
    });
    userData.forEach(u => {
      userOpt.push({
        label: u.name,
        value: u.id,
      });
    });

    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-12">
                    <h4 className="card-title mb-0">{ title }</h4>
                    <br />
                  </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <TextField
                        labelText="Role Name"
                        name="name"
                        onChange={this.handleChange}
                        value={name}
                        placeholder="Role Name"
                      />
                    </div>
                    <div>
                      <LoadingButton
                        className="btn btn-primary"
                        onClick={this.handleSubmit}
                        isLoading={this.state.isLoadingSave}
                        disabled={permission.length === 0 || name === ''}
                      >
                        {buttonLabel}
                      </LoadingButton>
                      &nbsp;
                      &nbsp;
                      <Link to="/admin/permissions/roles">
                        <button disabled={this.state.isLoadingSave} className="btn btn-default">
                          Back
                        </button>
                      </Link>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <ReactSelect
                        labelText="Permissions"
                        onChange={this.handlePermission}
                        value={permission}
                        options={permissionOpt}
                        isMulti
                      />
                    </div>
                    <div className="form-group">
                      <ReactSelect
                        labelText="Users"
                        onChange={this.handleUser}
                        value={user}
                        options={userOpt}
                        isMulti
                      />
                    </div>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RoleForm;
