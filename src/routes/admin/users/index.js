import React from 'react';
import AdminLayout from '../../../components/Layout/AdminLayout';

export default {
  path: '/users',
  children: [
    {
      path: '',
      async action() {
        const {
          default: UserPage,
        } = await import(/* webpackChunkName: 'UserPage' */ './components/UserPage');
        return {
          chunk: ['UserPage'],
          title: 'Users',
          component: (
            <AdminLayout>
              <UserPage />
            </AdminLayout>
          ),
        };
      },
    },
    {
      path: '/new',
      async action() {
        const {
          default: UserForm,
        } = await import(/* webpackChunkName: 'UserForm' */ './components/UserForm');
        return {
          chunks: ['UserForm'],
          title: 'Users New',
          component: (
            <AdminLayout>
              <UserForm />
            </AdminLayout>
          ),
        };
      },
    },
    {
      path: '/edit/:id',
      async action(context, params) {
        const {
          default: UserForm,
        } = await import(/* webpackChunkName: 'UserForm' */ './components/UserForm');
        return {
          chunks: ['UserForm'],
          title: 'Users Edit',
          component: (
            <AdminLayout>
              <UserForm params={params} />
            </AdminLayout>
          ),
        };
      },
    },
  ],
};
