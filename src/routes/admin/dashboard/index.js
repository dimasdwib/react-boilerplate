import React from 'react';
import AdminLayout from '../../../components/Layout/AdminLayout';

export default {
  path: '/dashboard',
  children: [
    {
      path: '',
      async action() {
        const {
          default: DashboardPage,
        } = await import(/* webpackChunkName: 'DashboardPage' */ './components/DashboardPage');
        return {
          chunks: ['DashboardPage'],
          title: 'Dashboard',
          component: (
            <AdminLayout>
              <DashboardPage />
            </AdminLayout>
          ),
        };
      },
    },
  ],
};
