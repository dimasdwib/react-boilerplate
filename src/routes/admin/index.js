import React from 'react';
import users from './users';
import dashboard from './dashboard';
import permissions from './permissions';
import NotFound from './NotFound';

export default {
  path: '/admin',
  children: [
    users,
    dashboard,
    permissions,
    {
      path: '(.*)',
      action: () => ({ component: <NotFound /> }),
    }
  ],

};
