/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import cookie from 'react-cookies';
import LoadingButton from '../../components/Button/LoadingButton';

class Login extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      errors: {},
      isLoadingLogin: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    const token = cookie.load('authToken');
    if (token) {
      window.location.href = '/admin/dashboard';
    }
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }

    const { username, password } = this.state;
    const data = {
      username,
      password,
    };

    this.setState({ isLoadingLogin: true, errors: {} });
    axios
      .post('auth/login', data)
      .then(res => {
        cookie.save('authToken', res.data.token, { path: '/' });
        // redirect to dashboard
        window.location.href = '/admin/dashboard';
      })
      .catch(err => {
        console.log(err);
        this.setState({
          isLoadingLogin: false,
          errors: {
            app: 'Invalid credential',
          },
        });
      });
  }

  render() {
    const { username, password, isLoadingLogin } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8">
              <div className="card-group">
                <div className="card p-4">
                  <form onSubmit={this.handleSubmit}>
                    <div className="card-body">
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fa fa-user" />
                          </span>
                        </div>
                        <input
                          value={username}
                          onChange={this.handleChange}
                          name="username"
                          className="form-control"
                          type="text"
                          placeholder="Username or Email"
                        />
                      </div>
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="fa fa-lock" />
                          </span>
                        </div>
                        <input
                          value={password}
                          onChange={this.handleChange}
                          name="password"
                          className="form-control"
                          type="password"
                          placeholder="Password"
                        />
                      </div>
                      {this.state.errors.app ? (
                        <div className="row">
                          <div className="col-12">
                            <p className="text-danger">
                              <i className="fa fa-exclamation-circle" />{' '}
                              {this.state.errors.app}{' '}
                            </p>
                          </div>
                        </div>
                      ) : null}
                      <div className="row">
                        <div className="col-6">
                          <LoadingButton
                            isLoading={isLoadingLogin}
                            onClick={this.handleSubmit}
                            className="btn btn-primary"
                          >
                            Login
                          </LoadingButton>
                        </div>
                        <div className="col-6 text-right">
                          <button
                            disabled={isLoadingLogin}
                            className="btn btn-link px-0"
                            type="button"
                          >
                            Forgot password?
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div
                  className="card text-white bg-primary py-5 d-md-down-none"
                  style={{ width: '44%' }}
                >
                  <div className="card-body text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua.
                      </p>
                      <button
                        className="btn btn-primary active mt-3"
                        type="button"
                      >
                        Register Now!
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
