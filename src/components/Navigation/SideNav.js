import React from 'react';
import Link from '../Link';

class SideNav extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li><Link to="/admin/dashboard"> Dashboard </Link></li>
          <li><Link to="/admin/users"> Users </Link></li>
        </ul>
      </div>
    );
  }
}

export default SideNav;