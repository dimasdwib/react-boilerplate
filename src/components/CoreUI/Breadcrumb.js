import React from 'react';

class Breadcrumb extends React.Component {
  render() {
    return (
      <ol className="breadcrumb">
        <li className="breadcrumb-item">Home</li>
        <li className="breadcrumb-item">
          <a href="#">Admin</a>
        </li>
        <li className="breadcrumb-item active">Dashboard</li>
        <li className="breadcrumb-menu d-md-down-none">
          <div className="btn-group" role="group" aria-label="Button group">
            <a className="btn" href="#">
              <i className="fa fa-comments"></i>
            </a>
            <a className="btn" href="./">
              <i className="fa fa-chart-line"></i>  Dashboard</a>
            <a className="btn" href="#">
              <i className="fa fa-cogs"></i>  Settings</a>
          </div>
        </li>
      </ol>
    );
  }
}

export default Breadcrumb;
