import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AdminLayout.css';
import DefaultLayout from '../CoreUI/DefaultLayout';
import AdminProvider from '../Provider/AdminProvider';

class AdminLayout extends React.Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
  };

  render() {
    return (
      <AdminProvider>
        <DefaultLayout>{this.props.children}</DefaultLayout>
      </AdminProvider>
    );
  }
}

export default withStyles(s)(AdminLayout);
