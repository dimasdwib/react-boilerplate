import React from 'react';
import { PulseLoader } from 'react-spinners';

class LoadingButton extends React.Component {
  render() {

    const btnProps = {...this.props};
    delete btnProps.isLoading;
    return (
      <button
        {...btnProps}
        disabled={this.props.disabled || this.props.isLoading}
      >
        {this.props.isLoading ? 'Loading' : this.props.children}
        <PulseLoader
          className="btn-loading"
          sizeUnit="px"
          size={7}
          color="white"
          loading={this.props.isLoading}
        />
      </button>
    );
  }
}

export default LoadingButton;
