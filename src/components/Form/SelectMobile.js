import React from 'react';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import find from 'lodash/find';

class SelectMobile extends React.Component {
  static propTypes = {
    options: PropTypes.array,
    className: PropTypes.string,
    onChange: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      value: !this.props.value || this.props.value === null ? '' : this.props.value,
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ...nextProps,
    });
  }

  onChange(e) {
    this.setState({
      value: e.target.value,
    });
    if (this.props.onChange) {
      const data = find(this.props.options, (o) => {
        if (o.value.toString() === e.target.value) {
          return o;
        }
      });
      // console.log(data, this.props.options, e.target.value);
      this.props.onChange(data);
    }
  }

  render() {
    const options = map(this.props.options, (data, i) => (
      <option key={i} value={data.value} > { data.label } </option>
    ));
    return (
      <select onChange={this.onChange} value={this.state.value} className={`${this.props.className} form-control`}>
        <option value={null} ></option>
        { options }
      </select>
    );
  }
}

export default SelectMobile;
