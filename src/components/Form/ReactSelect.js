import React from 'react';
import PropTypes from 'prop-types';
import MySelect from 'react-select';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './react-select.css';
import SelectMobile from './SelectMobile';

class ReactSelect extends React.Component {
  static propTypes = {
    isRequired: PropTypes.bool,
    errorText: PropTypes.string,
    warningText: PropTypes.string,
    successText: PropTypes.string,
    className: PropTypes.string,
  }

  validationClass() {
    const { errorText, warningText, successText } = this.props;

    if (errorText && errorText !== '') {
      return 'has-error';
    } else if (warningText && warningText !== '') {
      return 'has-warning';
    } else if (successText && successText !== '') {
      return 'has-success';
    }

    return null;
  }

  isMobile() {
    if (navigator.userAgent.match(/Android/i)
     || navigator.userAgent.match(/webOS/i)
     || navigator.userAgent.match(/iPhone/i)
     || navigator.userAgent.match(/iPad/i)
     || navigator.userAgent.match(/iPod/i)
     || navigator.userAgent.match(/BlackBerry/i)
     || navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    }

    return false;
  }

  exClass() {
    let exClass = [s.Select];

    if (this.props.className) {
      exClass.push(this.props.className);
    }

    exClass = exClass.join(' ');
    return exClass;
  }
  render() {
    const { labelText, isRequired, LabelText, floatingLabelText, hintText, errorText, warningText, successText, ...properties } = this.props;
    const a = isRequired ? ' *' : null;
    return (
      <div className={this.validationClass()}>
        { LabelText ? (<label className="control-label">{ LabelText } {a}</label>) : ''}
        { labelText ? (<label className="control-label">{ labelText } {a}</label>) : ''}
        { this.isMobile() ? <SelectMobile {...properties} className={this.exClass()} /> : <MySelect {...properties} className={this.exClass()} /> }
        { errorText && errorText !== '' ? (<span className="help-block"> {errorText} </span>) : '' }
        { warningText && warningText !== '' ? (<span className="help-block"> {warningText} </span>) : '' }
        { successText && successText !== '' ? (<span className="help-block"> {successText} </span>) : '' }
      </div>
    );
  }

}

export default withStyles(s)(ReactSelect);
