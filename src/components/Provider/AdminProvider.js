import React from 'react';
import PropTypes from 'prop-types';
import NotificationSystem from 'react-notification-system';
import Rodal from 'rodal';
import cookie from 'react-cookies';

export const AdminContext = React.createContext();

class AdminProvider extends React.Component {
  static resolveConfirm;
  static rejectConfirm;

  static propTypes = {
    children: PropTypes.element.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      confirmText: '',
      openModalConfirm: false,
    };
    this.notification = React.createRef();
    this.notify = this.notify.bind(this);
    this.confirm = this.confirm.bind(this);
    this.confirmOk = this.confirmOk.bind(this);
    this.confirmCancel = this.confirmCancel.bind(this);
  }

  componentDidMount() {
    const token = cookie.load('authToken');
    if (!token || token === null || token === '') {
      window.location.href = '/login';
    }
  }

  /*
   * Notification System
  */
  notify(level, message, opt = {}) {
    this.notification.current.addNotification({
      message,
      level,
      position: 'tr',
      title: level,
      ...opt,
    });
  }

  confirm(confirmText) {
    this.setState({ confirmText, openModalConfirm: true });
    return new Promise((resolve, reject) => {
      this.resolveConfirm = resolve;
      this.rejectConfirm = reject;
    });
  }

  confirmOk() {
    this.resolveConfirm();
    this.setState({ openModalConfirm: false });
  }

  confirmCancel() {
    this.rejectConfirm();
    this.setState({ openModalConfirm: false });
  }

  render() {
    const token = cookie.load('authToken');
    if (!token || token === null || token === '') {
      return null;
    }

    const adminProps = {
      notify: this.notify,
      confirm: this.confirm,
    };
    const componentWithProps = React.Children.only(
      React.cloneElement(this.props.children, adminProps),
    );
    return (
      <AdminContext.Provider value={adminProps}>
        <div>{componentWithProps}</div>
        <NotificationSystem ref={this.notification} />
        <div styles={{ zIndex: 1 }}>
          <Rodal
            visible={this.state.openModalConfirm}
            onClose={this.confirmCancel}
            closeMaskOnClick={false}
            animation="slideUp"
            height={200}
          >
            <div className="card-body">
              <h4 style={{ color: 'orange' }}>
                <i className="fa fa-exclamation-triangle fa-fw" /> Confirmation{' '}
              </h4>
              <br />
              <p>{this.state.confirmText}</p>
              <br />
              <div className="text-right">
                <button className="btn btn-primary" onClick={this.confirmOk}>
                  OK
                </button>
                &nbsp; &nbsp;
                <button
                  className="btn btn-default"
                  onClick={this.confirmCancel}
                >
                  Cancel
                </button>
              </div>
            </div>
          </Rodal>
        </div>
      </AdminContext.Provider>
    );
  }
}

export default AdminProvider;
